# translation of noosfero.po to
# Krishnamurti Lelis Lima Vieira Nunes <krishna@colivre.coop.br>, 2007.
# noosfero - Brazilian Portuguese translation
# Copyright (C) 2007,
# Forum Brasileiro de Economia Solidaria <http://www.fbes.org.br/>
# Copyright (C) 2007,
# Ynternet.org Foundation <http://www.ynternet.org/>
# This file is distributed under the same license as noosfero itself.
# Joenio Costa <joenio@colivre.coop.br>, 2008.
#
#
msgid ""
msgstr ""
"Project-Id-Version: 1.3~rc2-1-ga15645d\n"
"PO-Revision-Date: 2019-01-27 22:06+0000\n"
"Last-Translator: ssantos <ssantos@web.de>\n"
"Language-Team: Portuguese <https://hosted.weblate.org/projects/noosfero/"
"plugin-html5-video/pt/>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.5-dev\n"

msgid "A plugin to enable the video suport, with auto conversion for the web."
msgstr ""
"Um plugin para habilitar o suporte de video, com conversão automática para a "
"web."

msgid "Video Channel"
msgstr "Canal de Vídeo"

msgid "A video channel, where you can make your own web TV."
msgstr "Um canal de vídeo, no qual você pode criar sua própria TV na web."

msgid "Video (%s)"
msgstr "Vídeo (%s)"

msgid "Undefined"
msgstr "Indefinido"

msgid "Unknow error"
msgstr "Erro desconhecido"

msgid "No such file or directory \"%s\"."
msgstr "O ficheiro ou diretório \"%s\" não existe."

msgid "No output defined."
msgstr "Sem saída definida."

msgid "Unknown encoder \"%s\"."
msgstr "Codificador \"%s\" desconhecido."

msgid ""
"Error while opening encoder for output - maybe incorrect parameters such as "
"bit rate, frame rate, width or height."
msgstr ""
"Erro enquanto abria o codificador para gerar a saída - talvez por parâmetros "
"incorretos como bit rate, frame rate, largura ou altura."

msgid "Could not open \"%s\"."
msgstr "Não pude abrir \"%s\"."

msgid "Unsupported codec %{codec} for %{act} stream %{id}."
msgstr "Codec %{codec} não suportado para %{act} no fluxo %{id}."

msgid "Unable to find a suitable output format for %{file}."
msgstr "Incapaz de encontrar um formato de saída adequado para %{file}."

msgid "Invalid data found when processing input."
msgstr "Foi encontrado um dado inválido enquanto processava a entrada."

msgid "Success."
msgstr "Sucesso."

msgid "Sorry, your browser doesn&rsquo;t support video."
msgstr "Desculpe, o seu navegador não tem suporte a vídeo."

msgid "Please try the new %s or %s."
msgstr "Por favor tente o novo %s ou %s."

msgid "Download"
msgstr "Baixar"

msgid "This channel contains no videos yet"
msgstr "Este canal ainda não contém vídeos"

msgid "Quality options"
msgstr "Opções de qualidade"

msgid "Tags"
msgstr "Etiquetas"

msgid "Description"
msgstr "Descrição"

msgid "This channel has one video waiting to be converted"
msgid_plural "This channel has %d videos waiting to be converted"
msgstr[0] "Este canal tem um vídeo esperando para ser convertido"
msgstr[1] "Este canal tem %d vídeos esperando para serem convertidos"

msgid "%sº video in the queue"
msgstr "%sº vídeo na fila"

msgid "This video is being processed, it might take a while"
msgstr "O vídeo está sendo processado, isso pode demorar um pouco"

msgid "This video will be enqueued soon"
msgstr "O vídeo vai entrar na fila de processamento em breve"

msgid "Conversion couldn't be completed"
msgstr "Não foi possível processar esse vídeo"

msgid "Non video files"
msgstr "Ficheiros que não são vídeo"

msgid "This video is currently being processed. It might take a while."
msgstr ""
"Este vídeo está sendo processado neste momento, o que pode demorar um pouco."

msgid "Queued to generate the web version."
msgstr "Enfileirado para a geração das versões para a web."

msgid "This is the %sº video in the queue. Come back soon"
msgstr "Este é o %sº vídeo na fila de processamento. Volte em breve"

msgid "This video will be enqueued for conversion soon. Come back later."
msgstr "O vídeo será enfileirado para processamento em breve."

msgid "It was not possible to convert this video, contact an administator."
msgstr ""
"Não foi possível processar esse vídeo, entre em contato com um administrador."

msgid "Video conversion errors"
msgstr "Erros de conversão do vídeo"

msgid "Error while converting %{orig_type} to %{new_type}, %{size} size."
msgstr ""
"Erro durante a conversão de %{orig_type} para %{new_type}, tamanho %{size}."

msgid "Code %s"
msgstr "Código %s"

msgid "display full output"
msgstr "Mostrar toda a saída"
